<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?><!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
<?php astra_head_top(); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">

<?php wp_head(); ?>
<?php astra_head_bottom(); ?>
    <!-- Favicon -->
    <link href="https://www.adelielinux.org/assets/images/icons/gen_polyguin_color_favicon.ico" rel="shortcut icon">
    <!-- CSS -->
    <link href="https://www.adelielinux.org/assets/plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="https://www.adelielinux.org/assets/plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="https://www.adelielinux.org/assets/plugins/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
    <link href="https://www.adelielinux.org/assets/plugins/magnific-popup/magnific-popup.min.css" rel="stylesheet">
    <link href="https://www.adelielinux.org/assets/plugins/sal/sal.min.css" rel="stylesheet">
    <link href="https://www.adelielinux.org/assets/css/theme.css" rel="stylesheet">
    <!-- Fonts/Icons -->
    <link href="https://www.adelielinux.org/assets/plugins/font-awesome/css/all.css" rel="stylesheet">
    <link href="https://www.adelielinux.org/assets/plugins/themify/themify-icons.min.css" rel="stylesheet">
    <link href="https://www.adelielinux.org/assets/plugins/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    <!-- Blog Overrides -->
    <style>
        .entry-content a { color: #0170B9; }
    </style>
</head>

<body <?php astra_schema_body(); ?> <?php body_class(); ?>>
<?php astra_body_top(); ?>
<?php wp_body_open(); ?>
<div 
<?php
	echo astra_attr(
		'site',
		array(
			'id'    => 'page',
			'class' => 'hfeed site',
		)
	);
	?>
>
	<a class="skip-link screen-reader-text" href="#content"><?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?></a>
	<?php 
	astra_header_before(); 

	astra_header(); 

	astra_header_after();

	astra_content_before(); 
	?>
	<div id="content" class="site-content">
		<div class="ast-container">
		<?php astra_content_top(); ?>
