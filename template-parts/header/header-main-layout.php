<?php
/**
 * Template for Primary Header
 *
 * The header layout 2 for Astra Theme. ( No of sections - 1 [ Section 1 limit - 3 )
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @see https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package     Astra
 * @author      Astra
 * @copyright   Copyright (c) 2020, Astra
 * @link        https://wpastra.com/
 * @since       Astra 1.0.0
 */

?>

<!-- Header -->
<div class="header right sticky absolute-dark">
    <div class="container">
        <!-- Logo -->
        <div class="header-logo">
            <h3>
                <a href="https://www.adelielinux.org/">
                    <img
                        class="logo-dark"
                        src="https://www.adelielinux.org/assets/images/gen_polylogo_black_mono_x54.png"
                        alt=""
                        onmouseover="this.src='https://www.adelielinux.org/assets/images/gen_polylogo_black_color_x54.png'"
                        onmouseout="this.src='https://www.adelielinux.org/assets/images/gen_polylogo_black_mono_x54.png'"
                    />
                    <img
                        class="logo-light"
                        src="https://www.adelielinux.org/assets/images/gen_polylogo_white_mono_x54.png"
                        alt=""
                        onmouseover="this.src='https://www.adelielinux.org/assets/images/gen_polylogo_white_color_x54.png'"
                        onmouseout="this.src='https://www.adelielinux.org/assets/images/gen_polylogo_white_mono_x54.png'"
                    />
                </a>
            </h3>
        </div>
        <!-- Menu -->
        <div class="header-menu">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                    <ul class="nav-dropdown">
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/about/">The Adélie Way</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/desktop/">For Desktop</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/servers/">For Servers</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/faq/">Common Questions</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://www.adelielinux.org/download/">Download</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://www.adelielinux.org/screenshots/">Screenshots</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Help</a>
                    <ul class="nav-dropdown">
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/quickstart/">Getting Started</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://help.adelielinux.org/">Documentation</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/enterprise/">Enterprise Support</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/contribute/">Donate</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Software</a>
                    <ul class="nav-dropdown">
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://pkg.adelielinux.org/">Available Packages</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://gcompat.org/">"gcompat" Library</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://git.adelielinux.org/groups/adelie/-/issues">Issues & Bug Reports</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Community</a>
                    <ul class="nav-dropdown">
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/contact/">Contact Adélie</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://lists.adelielinux.org/archive/">Mailing Lists</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/contact/irc.html">IRC Web Client</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/contact/social.html">Social Media</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://blog.adelielinux.org/">Blog & Press Releases</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/standards/">Community Standards</a></li>
                    </ul>
                </li>
<!--
                <li class="nav-item">
                    <a class="nav-link" href="#">Research</a>
                    <ul class="nav-dropdown">
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/hpc/">HPC Deployments</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/partners/">Collaborations</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/whitepapers/">White Papers</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/publications/">Publications</a></li>
                        <li class="nav-dropdown-item"><a class="nav-dropdown-link" href="https://www.adelielinux.org/students/">For Students</a></li>
                    </ul>
                </li>
-->
                <li class="nav-item">
                    <a class="nav-link" href="https://git.adelielinux.org/">Git</a>
                </li>
            </ul>
        </div>
        <!-- Menu Toggle -->
        <button class="header-toggle">
            <span></span>
        </button>
    </div>
    <!-- end container -->
</div>
<!-- end Header -->

<div class="section-xs" style="padding-top: 100px; padding-bottom: 0px;">
    <div class="container text-center">
        <h1 class="font-family-secondary"><a href="/">The Adélie Chronicle</a></h1>
    </div><!-- end container -->
</div>
