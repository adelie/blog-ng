The **Adélie Linux WordPress Theme** is derived from [Astra](https://wpastra.com/) `v3.6.5` and is licensed under GPLv2 or later in accordance with applicable terms.

![Adélie Linux WordPress Theme](meta/adelie-theme-chooser.png)

This theme is running at https://blog.adelielinux.org/ which is the official Adélie Linux blog.

![Adélie Linux WordPress Theme Example Post](meta/adelie-theme-post.png)

# Installation

```sh
# on Sentry
cd $HOME/services/blog.adelielinux.org/wordpress/wp-content/themes/
git clone
```

Then activate the theme in the admin console.

# Updates

```sh
# on Sentry
cd $HOME/services/blog.adelielinux.org/wordpress/wp-content/themes/blog-ng/
git pull
```

Changes are applied instantly.

# Maintenance

Upstream source code ("initial import" was from released version):

    https://github.com/brainstormforce/astra

TODO:

  * Checkout v3.6.5, figure out how to "build" upstream from scratch
    (e.g. node container, grunt, ...)

  * Try apply all patches (i.e. not first commit) onto that, and
    if successful (parity with current site) write build scripts

  * Rebase to latest stable upstream tag, test, etc.
